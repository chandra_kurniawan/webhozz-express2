const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const routes = require('./src/http/route/route');
const CORSMiddleware = require('./src/http/middleware/CORS.middleware');
const responseMiddleware = require('./src/http/middleware/response.middleware');

const env = require('dotenv');
env.config();

app.use(bodyParser.json())
app.use(CORSMiddleware);

app.set('json spaces', 2)

app.use('/api', routes);
app.use(responseMiddleware);


app.listen(process.env.PORT, () => {
    console.log(`Server is listening on port ${process.env.PORT} with Application Name: ${process.env.APPLICATION_NAME}`);
  });

app.on('error', onError);
app.on('listening', onListening);

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = app.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}
