module.exports = async function responseMiddleware (req, res, next){
    if(req.businessLogic){
        console.log('businesslogic',req.businessLogic)
        res.header("Content-Type",'application/json');
        res.status(req.businessLogic.code).send(req.businessLogic)
    }
}
