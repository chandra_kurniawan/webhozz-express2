const responseHelper = require('../../helper/response.helper');
const jwt = require('jsonwebtoken');
const {authen} = require('../../../model/index')



class AuthController {
    async auth(req, res, next) {
        try {
            const { username, password } = req.body
            var check=await authen.findOne({
                where:{
                    username,
                    password
                },
                attributes:{
                    include:[
                        ['id','uid'],
                        'username',
                        'age',
                        'alamat'
                    ]
                }
            })
            // var check=true
            if(check){
                const token = jwt.sign({
                    uid:check.id,
                    username:check.username,
                    age:check.age,
                    alamat:check.alamat
                }, process.env.SECRET, { expiresIn: '1h' });
                res.status(200).json({
                    message:'berhasil login',
                    token
                })
            }else{
                res.status(404).json({
                    message:'gagal login'
                })
            }
            // if (username === 'tata' && password === '12345678') {
            //     const token = jwt.sign({
            //         uid: 987654321
            //     }, process.env.SECRET, { expiresIn: '1h' });
            //     var a = await responseHelper({
            //         "code": 200,
            //         "message": "Attempt Authentication Success",
            //         "entity": "authentication",
            //         "state": "attemptAuthenticationSuccess",
            //         "data": {
            //             "token": token
            //         }
            //     })
            //     next()
            //     res.status(200).send(a)
            // } else if (username === 'dona' && password === 'juventus') {
            //     const token = jwt.sign({
            //         uid: 123498765
            //     }, process.env.SECRET, { expiresIn: '1h' });
            //     var a = await responseHelper({
            //         "code": 200,
            //         "message": "Attempt Authentication Success",
            //         "entity": "authentication",
            //         "state": "attemptAuthenticationSuccess",
            //         "data": {
            //             "token": token
            //         }
            //     })
            //     // next()
            //     return res.status(200).send(a)
            // } else {
            //     var a = await responseHelper({
            //         "code": 401,
            //         "message": "Username or password invalid",
            //         "entity": "authentication",
            //         "state": "attemptAuthenticationError",
            //     })
            //     // next()
            //     return res.status(200).send(a)
            // }
        } catch (error) {
            console.log(error)
            var a= await responseHelper({
                "code": 500,
                "entity": "authentication",
                "state": "attemptAuthenticationError",
            })
            next()
            return res.status(400).send(a)
        }
    }
    // async auth(req,res){
    //     try{
    //         const { username, password } = req.body
    //         if (username === 'tata' && password === '12345678') {
    //             const token = jwt.sign({
    //                 uid: 987654321
    //             }, process.env.SECRET, { expiresIn: '1h' });
    //             req.businessLogic = await responseHelper({
    //                 "code": 200,
    //                 "message": "Attempt Authentication Success",
    //                 "entity": "authentication",
    //                 "state": "attemptAuthenticationSuccess",
    //                 "data": {
    //                     "token": token
    //                 }
    //             })
    //             var a=await responseHelper({
    //                 "code": 200,
    //                 "message": "Attempt Authentication Success",
    //                 "entity": "authentication",
    //                 "state": "attemptAuthenticationSuccess",
    //                 "data": {
    //                     "token": token
    //                 }
    //             })
    //             return res.status(200).send(a)
    //         }else{
    //             var a=await responseHelper({
    //                 "code": 401,
    //                 "message": "Username or password invalid",
    //                 "entity": "authentication",
    //                 "state": "attemptAuthenticationError",
    //             })
    //             return res.status(200).send(a)
    //         }

    //     }catch(error){
    //         return res.status(400).send({
    //             error,
    //             msg:'error'
    //         })
    //     }

    // }
}

module.exports = AuthController
